## Seamly Stylelint-Config

This is the default stylelint-config for `@seamly/*` implementations.

## Installation

```
yarn add -D @seamly/stylelint-config
// or
npm install -D @seamly/stylelint-config
```

## Usage

This package can be used as `@seamly/stylelint-config` following the Stylelint documentation on [Extending configurations](https://stylelint.io/user-guide/configure#extends).

### Within `.stylelintrc.js`

```
module.exports = {
  ...
  extends: [
    '@seamly/stylelint-config'
  ]
  ...
}
```
