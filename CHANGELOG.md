# Seamly Stylelint Config Changelog

## [Unreleased]

## 3.1.2 (5 September 2024)

- Updated dependencies (minor and patch updates)

## 3.1.1 (2 February 2023)

- Removed deprecated `stylelint-config-prettier`

## 3.1.0 (2 February 2023)

- Changed stylelint peerDependency to `stylelint@^16.2.1`
- Updated stylelint plugins
- Updated `@seamly/eslint-config` to `^3.1.0`
- Updated `@seamly/prettier-config` to `^3.1.0`

## 3.0.3 (2 November 2023)

- Changed stylelint peerDependency to `stylelint@^15.11.0`
- Updated `@seamly/eslint-config` to `3.0.2`

## 3.0.2 (5 October 2023)

- Changed stylelint peerDependency to `stylelint@^15.10.3`
- Updated `prettier` to `3.0.3`
- Updated `@seamly/eslint-config` to `3.0.1`
- Updated `@seamly/prettier-config` to `3.0.3`
- Updated `stylelint-config-standard-scss` to `11.0.0`

## 3.0.1 (8 August 2023)

- Removed deprecated stylelint rules

## 3.0.0 (7 August 2023)

- Changed stylelint peerDependency to `stylelint@^15.10.2`
- Updated stylelint plugins
- Updated `prettier` to `3.0.x`
- Updated `@seamly/eslint-config` to `3.0.x`
- Updated `@seamly/prettier-config` to `3.0.x`

## 2.0.0 (10 June 2022)

- Updated stylelint to `^14.9.0`

## 1.1.1 (24 August 2021)

- Changed stylelint peerDependency to `stylelint@^13.13.1`
- Changed `declaration-property-value-blacklist` to `declaration-property-value-disallowed-list` to reflect deprecation

## 1.1.0 (10 August 2021)

- Updated packages to latest non-breaking-changes versions

## 1.0.1 (11 May 2021)

- Disabled 'no-descending-specificity' rule

## 1.0.0 (22 April 2021)
